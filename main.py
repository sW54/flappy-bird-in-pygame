import pygame
import sys
import random
import time
from Unit import Unit
from Neat import Neat
from Network import Network

# scales the window to account for smaller (laptop) screens
if sys.platform == 'win32':
    import ctypes
    try:
        ctypes.windll.user32.SetProcessDPIAware()
    except AttributeError:
        pass


pygame.init()
screen = pygame.display.set_mode((540, 960))
clock = pygame.time.Clock()

blue_bird_downflap = pygame.transform.scale(pygame.image.load('assets/bluebird-downflap.png').convert_alpha(), (58, 39))
blue_bird_midflap = pygame.transform.scale(pygame.image.load('assets/bluebird-midflap.png').convert_alpha(), (58, 39))
blue_bird_upflap = pygame.transform.scale(pygame.image.load('assets/bluebird-upflap.png').convert_alpha(), (58, 39))
blue_bird_frames = (blue_bird_downflap, blue_bird_midflap, blue_bird_upflap)

red_bird_downflap = pygame.transform.scale(pygame.image.load('assets/redbird-downflap.png').convert_alpha(), (58, 39))
red_bird_midflap = pygame.transform.scale(pygame.image.load('assets/redbird-midflap.png').convert_alpha(), (58, 39))
red_bird_upflap = pygame.transform.scale(pygame.image.load('assets/redbird-upflap.png').convert_alpha(), (58, 39))
red_bird_frames = (red_bird_downflap, red_bird_midflap, red_bird_upflap)


class Player(object):

    def __init__(self, u=None, frames=blue_bird_frames):
        self._velocity = 0
        self._rotation_index = 1
        self._frames = frames
        self._surface = self._frames[self._rotation_index]
        self._rect = self._surface.get_rect(center=(100, 480))
        self._unit = u

        self._is_alive = True
        self._time_survived = 0
        self._h_distance_to_the_pipe_left = 0
        self._h_distance_to_the_pipe_right = 0
        self._v_distance_to_top_pipe = 0
        self._v_distance_to_bottom_pipe = 0

        self._has_jumped = False

    @property
    def unit(self):
        return self._unit

    @unit.setter
    def unit(self, new_unit: Unit):
        self._unit = new_unit

    @property
    def is_alive(self):
        return self._is_alive

    @is_alive.setter
    def is_alive(self, state):
        self._is_alive = state

    @property
    def has_jumped(self):
        return self._has_jumped

    @has_jumped.setter
    def has_jumped(self, val):
        self._has_jumped = val

    @property
    def velocity(self):
        return self._velocity

    @velocity.setter
    def velocity(self, new_velocity):
        self._velocity = new_velocity

    @property
    def surface(self):
        return self._surface

    @surface.setter
    def surface(self, new_surface):
        self._surface = new_surface

    @property
    def rect(self):
        return self._rect

    @rect.setter
    def rect(self, new_rect):
        self._rect = new_rect

    @property
    def rotation_index(self):
        return self._rotation_index

    @rotation_index.setter
    def rotation_index(self, new_rotation_index):
        self._rotation_index = new_rotation_index

    @property
    def time_survived(self):
        return self._time_survived

    @time_survived.setter
    def time_survived(self, val):
        self._time_survived = val

    def set_frames(self, f):
        self._frames = f

    def check_collision(self, pipes):
        for pipe in pipes:
            if self._rect.colliderect(pipe):
                return False
        if self._rect.top <= -30 or self._rect.bottom >= 836:
            return False
        return True

    def animate(self):
        self._surface = self._frames[self._rotation_index]
        self._rect = self._surface.get_rect(center=(100, self._rect.centery))

    def rotate(self):
        return pygame.transform.rotozoom(self._surface, -self._velocity * 3, 1)

    def apply_gravity(self):
        self._velocity += gravity

    def blit(self, el_bird):
        screen.blit(el_bird, self._rect)

    def look(self, pipes):
        if next_pipe:
            self._h_distance_to_the_pipe_left = self._rect.right - next_pipe.left
            self._h_distance_to_the_pipe_right = self._rect.right - next_pipe.right
            closest_top_pipe, closest_bot_pipe = get_next_vertical_pipe(pipes)
            if closest_top_pipe:
                self._v_distance_to_top_pipe = self._rect.top - closest_top_pipe.bottom
            if closest_bot_pipe:
                self._v_distance_to_bottom_pipe = self._rect.bottom - closest_bot_pipe.top
        else:
            self._h_distance_to_the_pipe_left = 500
            self._h_distance_to_the_pipe_right = 580
            self._v_distance_to_top_pipe = 80
            self._v_distance_to_bottom_pipe = 80

    @property
    def inputs(self):
        return [self._h_distance_to_the_pipe_left, self._h_distance_to_the_pipe_right, self._v_distance_to_top_pipe,
                self._v_distance_to_bottom_pipe, self._velocity]

    def jump(self):
        self.velocity = 0
        self.velocity -= 8.5

    def net_result(self):
        net = Network(self.unit.get_genome(), input_size, output_size)
        net.set_input(self.inputs)
        return net.calculate()


def draw_floor():
    screen.blit(floor_surface, (floor_x_pos, 836))
    screen.blit(floor_surface, (floor_x_pos + 540, 836))


def create_pipe():
    random_pipe_position = random.randrange(380, 640, 20)
    bottom_pipe = pipe_surface.get_rect(midtop=(640, random_pipe_position))
    top_pipe = pipe_surface.get_rect(midbottom=(640, random_pipe_position - 240))
    return bottom_pipe, top_pipe


def get_next_pipe(pipes):
    for pipe in pipes:
        if pipe.left > 5:
            return pipe


def get_next_vertical_pipe(pipes):
    if not next_pipe:
        return -1
    closest_top, closest_bot = None, None
    for pipe in pipes:
        if pipe.center[0] - 10 < next_pipe.center[0] < pipe.center[0] + 10:
            if pipe.bottom >= 880:
                if not closest_bot:
                    closest_bot = pipe
                    continue
                if closest_bot.left > pipe.left:
                    closest_bot = pipe
            else:
                if not closest_top:
                    closest_top = pipe
                    continue
                if closest_top.left > pipe.left:
                    closest_top = pipe
    return closest_top, closest_bot


def move_pipes(pipes):
    for pipe in pipes:
        pipe.centerx -= 3
    return pipes


def draw_pipes(pipes):
    for pipe in pipes:
        if pipe.bottom >= 880:
            screen.blit(pipe_surface, pipe)
        else:
            flipped_pipe = pygame.transform.flip(pipe_surface, False, True)
            screen.blit(flipped_pipe, pipe)


def delete_obsolete_pipes(pipes, pipe_scores):
    to_remove = 0
    i = 0
    for pipe in pipes:
        if pipe.centerx < -10:
            if pipe.bottom >= 880:
                pipe_scores.pop(0)
            to_remove += 1
        i += 1
    for i in range(to_remove):
        pipes.pop(0)


def score_display():
    score_surface = game_font_large.render(str(score), True, (255, 255, 255))
    score_rect = score_surface.get_rect(center=(270, 65))
    screen.blit(score_surface, score_rect)


def update_score(scr, pipes, pipe_scores):
    for i in range(0, len(pipe_scores)):
        if pipes[i * 2].bottom >= 880 and pipes[i * 2].centerx < 100 and pipe_scores[i] == 0:
            scr += 1
            pipe_scores[i] = 1
    return scr


def show_generation():
    high_score_surface = game_font_large.render("Generation: " + str(generation), True, (30, 30, 30))
    high_score_rect = high_score_surface.get_rect(center=(270, 900))
    screen.blit(high_score_surface, high_score_rect)


def show_player_count():
    high_score_surface = game_font_small.render("LEFT ALIVE: " + str(player_count), True, (30, 30, 30))
    high_score_rect = high_score_surface.get_rect(center=(270, 935))
    screen.blit(high_score_surface, high_score_rect)


def draw_network(p, output_clr):
    BLUE = (0, 150, 153, 120)
    target_rect = pygame.Rect((295, 250), (0, 0)).inflate(450, 300)
    shape_surf = pygame.Surface(target_rect.size, pygame.SRCALPHA)
    net = Network(p.unit.get_genome(), input_size, output_size)
    st = str(net)
    nodes = st.split("|")[0].split(";")
    nodes.pop(-1)
    cons = st.split("|")[1].split(";")
    cons.pop(-1)
    x, y = 0, 0
    last_x = -1
    i = 0
    node_coordinates = {}
    while i < len(nodes):
        node, x1 = nodes[i], nodes[i + 1]
        if last_x == x1:
            y += 60
        else:
            y = 20
            x += 120
            last_x = x1
        node_coordinates[node] = (20 + x, 20 + y)
        if x1 != '10':
            pygame.draw.circle(shape_surf, BLUE, (20 + x, 20 + y), 20)
        else:
            pygame.draw.circle(shape_surf, output_clr, (20 + x, 20 + y), 20)
        screen.blit(shape_surf, target_rect)
        i += 2
    for i in range(0, len(cons), 3):
        from_, to_, w = cons[i], cons[i + 1], cons[i + 2]
        coord1 = node_coordinates[from_]
        coord2 = node_coordinates[to_]
        pygame.draw.line(shape_surf, BLUE, coord1, coord2, 2)
        screen.blit(shape_surf, target_rect)
        i += 3


def start_next_generation():
    print(">>>>>>>>>> GENERATION ", generation, " <<<<<<<<<<")
    if generation > 1:
        neat.evolve()
    new_players = []

    for unit in neat.get_units():
        # if generation == 1:  # initial mutations to speed things up. can be skipped.
        #     unit.get_genome().mutate_connection()
        #     unit.get_genome().mutate_connection()
        #     unit.get_genome().mutate()
        #     unit.get_genome().mutate()
        new_players.append(Player(unit))
    return new_players


def reset_game():
    global score, pipe_list, pipe_scores_list, SPAWNPIPE, player_count
    pipe_list = []
    pipe_scores_list = []
    score = 0
    pipe_list.extend(create_pipe())
    pipe_scores_list.append(0)
    SPAWNPIPE = pygame.USEREVENT
    pygame.time.set_timer(SPAWNPIPE, 1400)
    player_count = 100


def events():
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
        if event.type == SPAWNPIPE:
            delete_obsolete_pipes(pipe_list, pipe_scores_list)
            pipe_list.extend(create_pipe())
            pipe_scores_list.append(0)
        if event.type == BIRDFLAP:
            for plr in players:
                if plr.is_alive:
                    plr.rotation_index = (plr.rotation_index + 1) % 3
                    plr.animate()


# GAME VARIABLES
gravity = 0.25
game_active = True
score = 0

game_font_large = pygame.font.Font("04B_19.ttf", 50)
game_font_small = pygame.font.Font("04B_19.ttf", 30)

bg_surface = pygame.image.load("assets/background-day-ns.png").convert()
bg_surface = pygame.transform.scale(bg_surface, (540, 960))

floor_surface = pygame.image.load("assets/base.png").convert()
floor_surface = pygame.transform.scale(floor_surface, (540, 180))

floor_x_pos = 0

BIRDFLAP = pygame.USEREVENT + 1
pygame.time.set_timer(BIRDFLAP, 200)

pipe_surface = pygame.image.load("assets/pipe-green.png").convert()
pipe_surface = pygame.transform.scale(pipe_surface, (84, 514))

pipe_list = []
pipe_scores_list = []
next_pipe = None
SPAWNPIPE = pygame.USEREVENT
pygame.time.set_timer(SPAWNPIPE, 1400)

game_over_surface = pygame.transform.scale(pygame.image.load("assets/gameover.png").convert_alpha(), (309, 68))
game_over_rect = game_over_surface.get_rect(center=(270, 480))

input_size = 5
output_size = 2
player_count = 50
neat = Neat(input_size, output_size, player_count)

generation = 1
players = start_next_generation()

pipe_list.extend(create_pipe())
pipe_scores_list.append(0)

otc = (255, 255, 255, 100)
initiated = time.time()
random_player = players[-1]
random_player.set_frames(red_bird_frames)

while True:
    events()

    screen.blit(bg_surface, (0, 0))

    if game_active:
        # Bird
        all_dead = True

        for player in players:
            player.has_jumped = False
            if player.is_alive:
                all_dead = False
                if not player.check_collision(pipe_list):
                    player.is_alive = False
                    player_count -= 1
                    player.unit.score = round(player.time_survived, 4)
                nr = player.net_result()
                if nr[0] > nr[1]:
                    player.jump()
                    player.has_jumped = True
                player.apply_gravity()
                rotated = player.rotate()
                player.rect.centery += int(player.velocity)
                player.blit(rotated)
                player.look(pipe_list)
                # player.time_survived += 0.1  # adds score based on time survived, can have bad results
        if not random_player.is_alive:
            for k in range(len(players) - 1, 0, -1):
                player = players[k]
                if player.is_alive:
                    random_player = player
                    random_player.set_frames(red_bird_frames)
                    break
        if not all_dead:
            now = time.time()
            if random_player.has_jumped:
                otc = (255, 77, 77, 100)
                initiated = time.time()
            if now > initiated + 0.2:
                otc = (255, 255, 255, 100)
            draw_network(random_player, otc)
        if all_dead:
            generation += 1
            print("HIGH SCORE: ", score)
            reset_game()
            players = start_next_generation()

        # Pipes
        next_pipe = get_next_pipe(pipe_list)
        pipe_list = move_pipes(pipe_list)
        draw_pipes(pipe_list)

        # Score update
        temp = score
        score = update_score(score, pipe_list, pipe_scores_list)
        if score > temp:
            if score == 50:
                random_player.unit.get_genome().print_genome()
            for player in players:
                if player.is_alive:
                    player.time_survived += score
    else:
        screen.blit(game_over_surface, game_over_rect)

    # Floor
    floor_x_pos -= 1
    draw_floor()
    if floor_x_pos <= -540:
        floor_x_pos = 0

    score_display()
    show_player_count()
    show_generation()

    pygame.display.update()
    clock.tick(120)
